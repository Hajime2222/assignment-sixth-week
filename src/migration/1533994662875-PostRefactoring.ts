import { MigrationInterface, QueryRunner } from 'typeorm'

export class PostRefactoring1533994662875 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
        CREATE TABLE public.game (
        id serial NOT NULL,
        "name" varchar NOT NULL,
        color varchar NOT NULL,
        board json NOT NULL DEFAULT '[[null,null,null],[null,null,null],[null,null,null]]'::json,
        CONSTRAINT "PK_352a30652cd352f552fef73dec5" PRIMARY KEY (id)
        )
        WITH (
            OIDS=FALSE
        ) ;
    `)
  }
  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query('DELETE TABLE IF EXISTS public.game;')
  }
}
