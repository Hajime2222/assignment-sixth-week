import 'reflect-metadata'
import { createKoaServer } from 'routing-controllers'
import { createConnection } from 'typeorm'
import * as credentials from '../credentials/credentials'

import GameController from './controllers/GameController'

const app = createKoaServer({
  controllers: [GameController]
})

app.listen(4000, () => {
  createConnection({
    type: 'postgres',
    url: process.env.DATABASE_URL || credentials.database.url,
    database: 'assignment_week_6',
    entities: ['src/entities/*.ts']
  }).catch(e => {
    console.log(e)
  })
})
