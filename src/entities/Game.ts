import { Matches, MinLength } from 'class-validator'

import {
  BaseEntity,
  BeforeInsert,
  Column,
  Entity,
  PrimaryGeneratedColumn
} from 'typeorm'

type Color = 'red' | 'blue' | 'green' | 'yellow' | 'magenta'
type Mark = 'x' | 'o' | null
type Row = [Mark, Mark, Mark]
export type Board = [Row, Row, Row]

const emptyBoard: Board = [
  [null, null, null],
  [null, null, null],
  [null, null, null]
]

export const colorOptions: Color[] = [
  'red',
  'blue',
  'green',
  'yellow',
  'magenta'
]

@Entity()
class Game extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @MinLength(2, { message: 'Name must be longer than 2 characters' })
  @Column()
  name: string

  @Matches(/^(red|blue|green|yellow|magenta)$/, { message: 'Invalid Color' })
  @Column()
  color: Color

  @Column({
    type: 'json',
    default: emptyBoard
  })
  board: Board

  @BeforeInsert()
  setRandomColor() {
    this.color = colorOptions[Math.floor(Math.random() * colorOptions.length)]
  }
}

export default Game
