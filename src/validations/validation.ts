import { Board, colorOptions } from '../entities/Game'

export const isValidMoveAndMark = (board1: Board, board2: Board): boolean => {
  const moves = board2
    .map((row, y) => row.filter((cell, x) => board1[y][x] !== cell))
    .reduce((a, b) => a.concat(b))

  return moves.length === 1 && (moves[0] === 'x' || moves[0] === 'o')
}
