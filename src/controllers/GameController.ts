import {
  BadRequestError,
  Body,
  BodyParam,
  Get,
  HttpCode,
  JsonController,
  NotFoundError,
  Param,
  Post,
  Put
} from 'routing-controllers'
import Game from '../entities/Game'
import { isValidMoveAndMark } from '../validations/validation'

@JsonController('/games')
class GameController {
  @Get('/')
  async getAllGames() {
    return { games: await Game.find() }
  }

  @Post('/')
  @HttpCode(201)
  async createGame(@BodyParam('name', { required: true }) name: string) {
    return { game: await Game.create({ name }).save() }
  }

  @Put('/:id')
  async updateGame(@Param('id') id: number, @Body() nextState: Partial<Game>) {
    const game = await Game.findOne(id)

    if (!game) throw new NotFoundError('Game Not Found')
    if (nextState.board && !isValidMoveAndMark(game.board, nextState.board)) {
      throw new BadRequestError('Invalid Move')
    }

    const updatedGame = Game.merge(game, nextState)
    return { game: await updatedGame.save() }
  }
}

export default GameController
