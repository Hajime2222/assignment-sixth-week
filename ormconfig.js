const credentials = require('./credentials/credentials')
module.exports = {
  type: 'postgres',
  url: process.env.DATABASE_URL || credentials.database.url,
  database: 'assignment_week_6',
  entities: ['target/entities/*.ts'],
  migrations: ['target/migration/*.js'],
  cli: {
    migrationsDir: 'target/migration'
  }
}
